import Vuex from 'vuex';

/* eslint-disable arrow-body-style */
const createStore = () => {
  return new Vuex.Store({
    state: {
      albums: [],
    },
    mutations: {
      add(state, payload) {
        /* eslint-disable no-param-reassign */
        state.albums = payload;
      },
    },
  });
};

export default createStore;
