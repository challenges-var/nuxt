// const axios = require('axios');

module.exports = {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Nuxt-CMS',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' },
    ],
    script: [
      { src: 'https://unpkg.com/ionicons@4.1.2/dist/ionicons.js' },
      { src: 'https://aframe.io/releases/0.7.1/aframe.min.js' },
      { src: 'https://dist.3d.io/3dio-js/1.1.x/3dio.min.js' },
    ],
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  css: [
    // SCSS file in the project
    'element-ui/lib/theme-chalk/index.css',
    // 'bootstrap/dist/css/bootstrap.css',
    'bootstrap-vue/dist/bootstrap-vue.css',
    'assets/styles/main.scss',
  ],

  build: {
    /*
    ** Run ESLint on save
    */
    vendor: [
      'axios',
      'element-ui',
      'bootstrap',
      'bootstrap-vue',
    ],

    extend(config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },

  plugins: [
    'plugins/plugins.js',
  ],

  modules: [
    ['storyblok-nuxt', {
      accessToken: process.env.NODE_ENV === 'production' ? '58DPImfjD2NQG1FaBWmgCgtt' : '70zQLlKKOPGpQG0LbmAQpwtt',
      cacheProvider: 'memory',
    }],
  ],

  // generate: {
  //   /* eslint-disable arrow-body-style */
  //   routes: () => {
  //     return axios.get(`https://api.storyblok.com/v1/cdn/stories?version=published&token=58DPImfjD2NQG1FaBWmgCgtt&starts_with=blog&cv=${Math.floor(Date.now() / 1e3)}`)
  //       .then((res) => {
  //         const blogPosts = res.data.stories.map(bp => bp.full_slug);
  //         return ['/', 'blog', 'about', ...blogPosts];
  //       });
  //   },
  // },
};
