import Vue from 'vue';

import ElementUI from 'element-ui';
import BootstrapVue from 'bootstrap-vue';
// import io3d from '3dio';

Vue.use(ElementUI);
Vue.use(BootstrapVue);
// Vue.use(io3d);
